// ==UserScript==
// @name KIT ZAR auto login for downloads
// @author Christoph
// @version 4
// @icon http://www.zar.kit.edu/img/intern/favicon.ico
// @include http://www.zar.kit.edu/490.php?ID=*&download=*
// @run-at document-idle
// @downloadURL https://gitlab.com/chrisbr/zar-auto-download-login-userscript/raw/master/zar-auto-download-login.user.js
// @updateURL https://gitlab.com/chrisbr/zar-auto-download-login-userscript/raw/master/zar-auto-download-login.user.js
// ==/UserScript==

/////////////////////////////////////////////////////////////////////////
////////// Password list (add missing passwords here) ///////////////////
/////////////////////////////////////////////////////////////////////////

var passwords = [
	["Datenschutzrecht","marsch"],
	["BGB","matz"],
	["Gewerblicher Rechtsschutz und Urheberrecht","dreier"],
  ["Tutorium Datenschutzrecht","Personenbezug"],
  ["Urheberrecht","dreier"],
  ["Öffentliches Recht I - Grundlagen","marsch"]
];

/////////////////////////////////////////////////////////////////////////
///////////////////// Start of source code //////////////////////////////
/////////////////////////////////////////////////////////////////////////

document.getElementsByTagName("input").user.value = "student";

var lecture = document.getElementsByTagName("h1")[0].innerText;
print("Detected lecture: " + lecture);

var lecturePassword = "";

for (var i = 0; i < passwords.length; i++) {
    if (lecture.startsWith(passwords[i][0])) {
    	lecturePassword = passwords[i][1];
    }
}

if (lecturePassword == ""){
	print("No password for automatic download found!\n Edit the sourcecode to add it now.");
} else {
	document.getElementsByTagName("input").passwort.value = lecturePassword;
  document.getElementsByTagName("form")[0].submit();
  print("Started download!");
}

function print(text) {
	console.log("ZAR-AUTO-DOWNLOADER: " + text);
}